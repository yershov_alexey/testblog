-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 01 2014 г., 00:55
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `yaf_blog`
--
CREATE DATABASE IF NOT EXISTS `yaf_blog` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `yaf_blog`;

-- --------------------------------------------------------

--
-- Структура таблицы `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(255) NOT NULL,
  `title` varchar(1024) NOT NULL,
  `content` text NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `article`
--

INSERT INTO `article` (`id`, `author_name`, `title`, `content`, `date_create`) VALUES
(6, 'Мэт Зандстра', 'Абстрактные классы и интерфейсы', '1. Предполагаемая аудитория\r\n\r\nЭта статья предназначена для опытных PHP-программистов, которые хотят разобраться в том, как работать с типами классов в PHP 5 через приведение типов , абстрактные классы и интерфейсы. Предполагается, что читатели уже знакомы с объектно-ориентированным программированием, включая устройство самих классов и механизм наследования.\r\n\r\n2. Введение\r\n\r\nБольшинство PHP-программистов знают, что поддержка объектно-ориентированного программирования в Zend Engine II значительно улучшена. Три новые возможности - приведение типов, абстрактные классы и интерфейсы определяют возможности установки и проверки типов классов. В этой статье мы увидим, как эти возможности помогают писать более качественный код и предотвращать появление ошибок.\r\n\r\n3. Типы классов\r\n\r\nВ PHP существует 12 предопределенных типов данных, включая простые типы, например, целые и с плавающей точкой, комплексные типы (массивы и объекты) и специальные типы (ресурсы и NULL). Поскольку PHP поддерживает классы и объекты, у вас также есть возможность определять собственные типы. На самом деле, каждый раз при создании класса вы определяете новый тип данных.', '2014-09-30 16:17:55'),
(7, 'Иван Неизвестный', 'Ошибка Deprication или переход на версию PHP 5.3', 'В последнее время все больше и больше приходит писем от наших посетителей, которые установив последнюю версию PHP 5.3.1 получают в работающих проектах подобные ошибки:\r\n\r\nDeprecated: Function eregi() is deprecated in ...\r\n\r\nНеприятная ситуация, когда проект работает и застает врасплох. Хорошо, что большинство хостеров не так быстро делают переход с версии PHP 5.2. на PHP 5.3. Но надолго ли?\r\n\r\nЧто такое DEPRECATED? Это некоторые параметры, функции и возможности, которые обозначены как НЕИСПОЛЬЗУЕМЫЕ и в последующих версиях будут УДАЛЕНЫ.\r\n\r\nДля начала конечно можно временно (повторюсь временно) можно выключить в файле php.ini данные предупреждения что бы сайт восстановил работоспособность и не выдавали так же ошибки вывода заголовков Headers (популярная ошибка новичков headers already sent) В PHP 5.3. для этого добавлено 2 уровня ошибок (error level) E_DEPRECATED и E_USER_DEPRECATED, которые и управляют выводом предупреждений (Warning)\r\n\r\nПолный список изменений вы можете увидите на официальном сайте PHP Deprecated features in PHP 5.3.x\r\n\r\nСледующие директивы php.ini будут отмечены как НЕИСПОЛЬЗУЕМЫЕ (DEPRECATED) в PHP 5.3 и удалены в PHP 6.0.0\r\n\r\ndefine_syslog_variables - теперь всегда OFF.\r\n \r\nregister_globals - теперь всегда OFF. Наконец-то! =)\r\n \r\nregister_long_arrays - $HTTP_*_VARS навсегда уйдут в небытие.\r\n \r\nsafe_mode - безопасный режим. По умолчанию OFF.\r\n \r\nmagic_quotes_gpc \r\nmagic_quotes_runtime \r\nmagic_quotes_sybase - будут удалены при переходе версии.\r\n \r\nТеперь в php.ini файле нельзя будет применять коментарии, начинающиеся с #\r\n \r\nТак же следующие функции:\r\n\r\ncall_user_method() (используйте call_user_func())\r\n \r\ncall_user_method_array() (используйте call_user_func_array())\r\n \r\ndefine_syslog_variables()\r\n \r\ndl()\r\n \r\nereg() (используйте preg_match())\r\n \r\nereg_replace() (используйте preg_replace())\r\n \r\neregi() (используйте preg_match() с модификатором ''i'')\r\n \r\neregi_replace() (используйте preg_replace() с модификатором ''i'')\r\n \r\nset_magic_quotes_runtime() и ее синоним(alias) magic_quotes_runtime()\r\n \r\nsession_register() (используйте $_SESSION)\r\n \r\nsession_unregister() (используйте $_SESSION)\r\n \r\nsession_is_registered() (используйте $_SESSION)\r\n \r\nset_socket_blocking() (используйте stream_set_blocking())\r\n \r\nsplit() (используйте preg_split())\r\n \r\nspliti() (используйте preg_split() с модификатором ''i'')\r\n \r\nsql_regcase()\r\n \r\nmysql_db_query() (используйте mysql_select_db() и mysql_query())\r\n \r\nmysql_escape_string() (используйте mysql_real_escape_string())\r\n \r\nИ следующие возможности:\r\n\r\nВ new передача параметра по ссылке в переменную\r\n \r\nВызов call-time функций, методов и классов по ссылке\r\n \r\nИспользование {} для указания смещения внутри строки. Теперь это можно сделать с помощью [] (как в массиве)\r\n \r\nСледует позаботиться о замене функций аналогами, описанными выше и переписать скрипты.', '2014-09-30 19:26:21');

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `author_email` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `i_article_id` (`article_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `article_id`, `author_name`, `author_email`, `comment`, `date_create`) VALUES
(1, 7, 'Алексей', 'yershov.alexey@gmail.com', 'Отличная статья! Благодарю!', '2014-09-30 20:34:32'),
(2, 7, 'Василий Петрович', 'vasya@mail.ru', 'Ничего не понял...', '2014-09-30 20:48:17');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
