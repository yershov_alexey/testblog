<?php if (isset($data['article']) && !empty($data['article'])):?>
<h2><?=$data['article']->title?></h2>
Автор: <?= $data['article']->author_name; ?><br /><br />
<?= $data['article']->content; ?>
<?php endif; ?>
<br />
<br />
<?php if (isset($data['comments']) && !empty($data['comments'])): ?>
	<h3>Комментарии:</h3>
	<?php foreach($data['comments'] as $item): ?>
		<i><?= $item->author_name; ?></i>
		<?= mydate($item->date_create); ?>:<br />
		<?= $item->comment; ?><br />
		<hr>
	<?php endforeach; ?>
<?php else: ?>
	<h4>Комментарии отсутствуют</h4>
<?php endif;?>
<h3>Оставить комментарий:</h3>
<span style="color:red"><?php echo validation_errors(); ?></span>
<form method="post">
	<input type="hidden" name="article_id" value="<?= $data['article']->id; ?>"/>
	Имя автора:<br />
	<input name="author_name" value="<?= set_value('author_name'); ?>"/><br />
	Email:<br />
	<input name="author_email" value="<?= set_value('author_email'); ?>"/><br />
	Комментарий:<br />
	<textarea cols="40" rows="5" name="comment"><?= set_value('comment'); ?></textarea><br />
	<input type="submit" name="add_comment" value="Отправить" />
</form>