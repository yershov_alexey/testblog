<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Блог</title>
</head>
<body>

<div id="container">
    <?php $this->load->view('header'); ?>
    <div id="body">
        <? if(isset($data) && !empty($data['inner_view'])): ?>
            <?php $this->load->view($data['inner_view']); ?>
        <? endif; ?>
    </div>
    <?php $this->load->view('footer'); ?>

</div>

</body>
</html>