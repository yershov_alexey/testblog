<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model', 'model');
		$this->load->helper('blog_helper');
	}

	/**
	 * Главная страница - лента блога
	 */
	public function index()
	{
		$data['articles'] = $this->model->get_articles();
		$data['inner_view'] = 'main';
		$this->load->view('index', array('data' => $data));
	}

	/**
	 * Страница добавления статей
	 */
	public function add_article()
	{
		if ($this->input->post('add_article')) {
			$this->form_validation->set_rules('author_name', 'Имя автора', 'trim|required');
			$this->form_validation->set_rules('title', 'Название статьи', 'trim|required');
			$this->form_validation->set_rules('content', 'Содержимое статьи', 'trim|required');
			if ($this->form_validation->run() == true) {
				$article = array(
								'author_name' => $this->input->post('author_name'),
								'title' => $this->input->post('title'),
								'content' => $this->input->post('content'),
				);
				$this->model->add_article($article);
				redirect('/');
			}
		}
		$data['inner_view'] = 'add_article';
		$this->load->view('index', array('data' => $data));
	}

	/**
	 * Страница конкретной статьи
	 * @param $article_id
	 */
	public function article($article_id = null)
	{
		// Добавление комментария
		if ($this->input->post('add_comment')) {
			$this->form_validation->set_rules('author_name', 'Автор', 'trim|required');
			$this->form_validation->set_rules('author_email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('comment', 'Комментарий', 'trim|required');
			if ($this->form_validation->run() == true) {
				$comment = array(
					'article_id' => $this->input->post('article_id'),
					'author_name' => $this->input->post('author_name'),
					'author_email' => $this->input->post('author_email'),
					'comment' => $this->input->post('comment'),
				);
				$this->model->add_comment($comment);
				redirect('/main/article/' . $comment['article_id']);
			}
		}

		$data['article'] = $this->model->get_article($article_id);
		$data['comments'] = $this->model->get_comments($article_id);
		$data['inner_view'] = 'article';
		$this->load->view('index', array('data' => $data));
	}

}

/* End of file main.php */
/* Location: ./application/controllers/main.php */