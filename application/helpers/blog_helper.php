<?php

/**
 * Форматируем дату как нам нужно
 * @param $date_string
 * @return bool|string
 */
function mydate($date_string)
{
    $date = date_create($date_string);
    return date_format($date, 'Y-m-d');
}

/**
 * Получить превью статьи
 * @param $text
 * @param int $length
 * @return string
 */
function get_preview($text, $length = 300)
{
    $res = mb_substr($text, 0, $length);
    if (mb_strlen($text) > $length) {
        $pos = mb_strrpos($res, ' ');
        $res = mb_substr($res, 0, $pos) . '&hellip;';
    }
    return $res;
}