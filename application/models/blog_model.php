<?php

/**
 * Модель для блога (функции по работе со статьями и комментариями)
 * Class Blog_model
 */
class Blog_model extends CI_Model
{

	/**
	 * Получить все статьи
	 * @return mixed
	 */
	public function get_articles()
	{
		return $this->db->query('SELECT * FROM article')->result();
	}

    /**
     * Получить статью по id
     * @param null $id
     * @return null
     */
    public function get_article($id = null)
	{
        $id = (int) $id;
		if (empty($id)) {
			return null;
		}
		return $this->db->query('SELECT * FROM article WHERE id = ?', array($id))->row();
	}

	/**
	 * Добавить статью
	 * @param $article
	 */
	public function add_article($article)
	{
		$this->db->query("INSERT INTO article (author_name, title, content, date_create)
						  VALUES(?, ?, ?, NOW())",
			array($article['author_name'],
				$article['title'],
				$article['content'],
			));
	}

	/**
	 * Добавить комментарий
	 * @param $comment
	 */
	public function add_comment($comment)
	{
		$this->db->query("INSERT INTO comment (article_id, author_name, author_email, comment, date_create)
						  VALUES(?, ?, ?, ?, NOW())",
			array(
				$comment['article_id'],
				$comment['author_name'],
				$comment['author_email'],
				$comment['comment'],
			));
	}

	/**
	 * Получить комментарии к конкретной статье
	 * @param $article_id
	 * @return mixed
	 */
	public function get_comments($article_id = null)
	{
		if (empty($article_id)) {
			return null;
		}
		return $this->db->query('SELECT * FROM comment WHERE article_id = ?', array($article_id))->result();
	}

}